'''You are given an array of integers. 
 You should find the sum of the elements
 with even indexes (0th, 2nd, 4th...)
 then multiply this summed number
 and the final element of the array together.
 Don't forget that the first element has an index of 0.'''

def checkio(array):
    if len(array)>=1:
        lista = sum(array[::2])
        lista *= array[-1]
        
        return lista

    else:
        return 0


 if __name__ == '__main__':
     assert checkio([0, 1, 2, 3, 4, 5]) == 30, "(0+2+4)*5=30"
     assert checkio([1, 3, 5]) == 30, "(1+5)*5=30"
     assert checkio([6]) == 36, "(6)*6=36"
     assert checkio([]) == 0, "Empty"
