'''You are given a string with words and numbers separated
by whitespaces (one space). The words contains only letters.
You should check if the string contains three words in succession.'''

def checkio(words):
    contar = 0	
    corte = words.split()

    if corte >= 3:
        for i in corte:
            palabra = i #problem in this line T.T
            if palabra.isalpha():
                contar += 1
            else:
            	contar = 0
            if contar == 3:
                return True

    else:
        return False
    if contar == 3:
    	return True


if __name__ == '__main__':
    assert checkio(u"Hello World hello") == True, "Hello"
    assert checkio(u"He is 123 man") == False, "123 man"
    assert checkio(u"1 2 3 4") == False, "Digits"
    assert checkio(u"bla bla bla bla") == True, "Bla Bla"
    assert checkio(u"Hi") == False, "Hi"
